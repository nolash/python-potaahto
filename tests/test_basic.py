# standard imports
import unittest

# local imports
from potaahto.symbols import snake_and_camel


class TestFauna(unittest.TestCase):


    def test_snake_and_camel(self):
        k_stupid = {
            'fooBarBaz': 1,
            'barBazFoo': 2,
            'baz_foo_bar': 3,
            }
        
        k = snake_and_camel(k_stupid)

        self.assertEqual(k['fooBarBaz'], 1)
        self.assertEqual(k['barBazFoo'], 2)
        self.assertEqual(k['bazFooBar'], 3)

        self.assertEqual(k['foo_bar_baz'], 1)
        self.assertEqual(k['bar_baz_foo'], 2)
        self.assertEqual(k['baz_foo_bar'], 3)


if __name__ == '__main__':
    unittest.main()
